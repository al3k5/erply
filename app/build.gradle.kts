plugins {
    id("com.android.application")
    kotlin("android")
    kotlin("kapt")
    id("androidx.navigation.safeargs.kotlin")
    id("dagger.hilt.android.plugin")
    kotlin("plugin.serialization") version Versions.kotlin
}

android {
    compileSdk = 30
    // using 30.0.2 because 30.0.3 gives warnings about buildSrc
    buildToolsVersion = "30.0.2"

    defaultConfig {
        applicationId = "com.appstand.erply"
        minSdk = 24
        targetSdk = 30
        versionCode = 1
        versionName = "1.0"

        testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"
        vectorDrawables {
            useSupportLibrary = true
        }
    }

    buildTypes {
        getByName("release") {
            isMinifyEnabled = false
            proguardFiles(
                getDefaultProguardFile("proguard-android-optimize.txt"),
                "proguard-rules.pro"
            )
        }
    }
    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_1_8
        targetCompatibility = JavaVersion.VERSION_1_8
    }
    kotlinOptions {
        jvmTarget = "1.8"
    }
    buildFeatures {
        viewBinding = true
    }
}

dependencies {
    implementation("org.jetbrains.kotlin:kotlin-stdlib:${Versions.kotlin}")
    implementation("androidx.core:core-ktx:{${Versions.androidXCoreKtx}}")
    implementation("androidx.appcompat:appcompat:${Versions.appCompat}")
    implementation("com.google.android.material:material:${Versions.material}")
    implementation("androidx.navigation:navigation-fragment-ktx:${Versions.navigation}")
    implementation("androidx.navigation:navigation-ui-ktx:${Versions.navigation}")
    implementation("com.squareup.okhttp3:logging-interceptor:${Versions.okhttp}")
    implementation("com.squareup.okhttp3:okhttp:${Versions.okhttp}")
    implementation("com.squareup.retrofit2:retrofit:${Versions.retofit}")
    implementation("com.jakewharton.retrofit:retrofit2-kotlinx-serialization-converter:${Versions.serializationConverter}")
    implementation("androidx.lifecycle:lifecycle-viewmodel-ktx:${Versions.lifecycle}")
    implementation("org.jetbrains.kotlinx:kotlinx-serialization-json:${Versions.serialization}")
    kapt("com.google.dagger:hilt-android-compiler:${Versions.hilt}")
    implementation("com.google.dagger:hilt-android:${Versions.hilt}")
    implementation("com.jakewharton.timber:timber:${Versions.timber}")
    implementation("androidx.datastore:datastore-preferences:${Versions.datastore}")
    implementation("androidx.datastore:datastore-preferences-core:${Versions.datastore}")
    implementation("androidx.work:work-runtime-ktx:${Versions.workManager}")
    implementation("androidx.hilt:hilt-work:${Versions.workHilt}")
    kapt("androidx.hilt:hilt-compiler:${Versions.workHilt}")

    testImplementation("junit:junit:${Versions.junit}")
    testImplementation("androidx.arch.core:core-testing:${Versions.archTesting}")
    testImplementation("org.jetbrains.kotlinx:kotlinx-coroutines-test:${Versions.kotlinTest}")
    testImplementation("org.mockito:mockito-core:${Versions.mockito}")
    testImplementation("app.cash.turbine:turbine:${Versions.turbine}")

    androidTestImplementation("androidx.test.ext:junit:${Versions.androidXTestJunit}")
    androidTestImplementation("androidx.test.espresso:espresso-core:${Versions.espresso}")
}
