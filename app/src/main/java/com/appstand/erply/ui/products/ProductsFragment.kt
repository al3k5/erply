package com.appstand.erply.ui.products

import android.content.res.Configuration
import android.os.Bundle
import android.util.DisplayMetrics
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.GridLayoutManager
import androidx.work.ExistingPeriodicWorkPolicy
import androidx.work.PeriodicWorkRequestBuilder
import androidx.work.WorkManager
import com.appstand.erply.R
import com.appstand.erply.databinding.FragmentProductsBinding
import com.appstand.erply.ui.general.TopAppBarFragment
import com.appstand.erply.ui.products.adapter.ProductsAdapter
import com.appstand.erply.utils.RefreshTokenWorker
import com.appstand.erply.utils.TOKEN_WORK_NAME
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.FlowPreview
import kotlinx.coroutines.flow.debounce
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.launch
import timber.log.Timber
import java.util.concurrent.TimeUnit

@Suppress("DEPRECATION")
@FlowPreview
@AndroidEntryPoint
class ProductsFragment : Fragment() {

    companion object {
        private const val SEARCH_DEBOUNCE_TIME = 300L
    }

    private val viewModel: ProductsViewModel by viewModels()
    private var _binding: FragmentProductsBinding? = null

    private val binding get() = _binding!!
    private lateinit var adapter: ProductsAdapter
    private val topAppBarFragment: TopAppBarFragment by lazy {
        childFragmentManager.findFragmentById(R.id.topAppBarFragment) as TopAppBarFragment
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentProductsBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setup()
        observeViewModel()

        startWorkManager()
    }

    private fun startWorkManager() {
        val request =
            PeriodicWorkRequestBuilder<RefreshTokenWorker>(1, TimeUnit.HOURS)
                .build()
        WorkManager.getInstance(requireContext().applicationContext)
            .enqueueUniquePeriodicWork(TOKEN_WORK_NAME, ExistingPeriodicWorkPolicy.REPLACE, request)
    }

    private fun setup() {
        binding.recyclerView.layoutManager = GridLayoutManager(requireContext(), getSpanCount())
        adapter = ProductsAdapter()
        binding.recyclerView.adapter = adapter

        topAppBarFragment.searchText.debounce(SEARCH_DEBOUNCE_TIME)
            .onEach { viewModel.searchTermChanged(it) }
            .launchIn(lifecycleScope)
    }

    private fun observeViewModel() {
        lifecycleScope.launch {
            viewModel.state.onEach { state ->
                Timber.d(state.toString())
                adapter.submitList(state.productItems)
            }.launchIn(this)
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        binding.recyclerView.adapter = null
        _binding = null
    }

    private fun getSpanCount(): Int {
        val metrics = DisplayMetrics()
        requireActivity().windowManager.defaultDisplay.getMetrics(metrics)
        val yInches = metrics.heightPixels / metrics.ydpi
        val xInches = metrics.widthPixels / metrics.xdpi
        return viewModel.getSpanCount(
            resources.configuration.orientation == Configuration.ORIENTATION_LANDSCAPE,
            yInches,
            xInches
        )
    }
}
