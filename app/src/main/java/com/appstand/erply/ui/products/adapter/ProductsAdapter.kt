package com.appstand.erply.ui.products.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.appstand.erply.data.Product
import com.appstand.erply.databinding.ProductItemBinding

class ProductsAdapter :
    ListAdapter<ProductItem, ProductsAdapter.ProductViewHolder>(DiffCallback()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ProductViewHolder {
        val binding = ProductItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ProductViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ProductViewHolder, position: Int) {
        val product = getItem(position)
        holder.bind(product)

    }

    class ProductViewHolder(private val binding: ProductItemBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(item: ProductItem) {
            binding.productNameView.text = item.product.name.en
        }
    }

    private class DiffCallback : DiffUtil.ItemCallback<ProductItem>() {

        override fun areItemsTheSame(oldItem: ProductItem, newItem: ProductItem) =
            oldItem.product.id == newItem.product.id

        override fun areContentsTheSame(oldItem: ProductItem, newItem: ProductItem) =
            oldItem == newItem
    }
}

data class ProductItem(val product: Product)
