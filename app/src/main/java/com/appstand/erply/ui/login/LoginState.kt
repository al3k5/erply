package com.appstand.erply.ui.login

data class LoginState(
    val loading: Boolean = false,
    val idError: Boolean = false,
    val usernameError : Boolean = false,
    val passwordError: Boolean = false
)
