package com.appstand.erply.ui.products

import com.appstand.erply.ui.products.adapter.ProductItem

data class ProductsState(
    val loading: Boolean = false,
    val currentIsBlankSearchResult: Boolean = false,
    val productItems: List<ProductItem> = listOf()
)
