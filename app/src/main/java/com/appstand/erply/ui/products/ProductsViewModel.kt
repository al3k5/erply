package com.appstand.erply.ui.products

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.appstand.erply.data.Product
import com.appstand.erply.ui.products.adapter.ProductItem
import com.appstand.erply.usecase.ObserveUserUseCase
import com.appstand.erply.usecase.SearchProductsFromApiUseCase
import com.appstand.erply.usecase.UseCaseResult
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.launch
import timber.log.Timber
import javax.inject.Inject
import kotlin.math.sqrt

@HiltViewModel
class ProductsViewModel @Inject constructor(
    private val searchProductsFromApiUseCase: SearchProductsFromApiUseCase,
    private val observeUserUseCase: ObserveUserUseCase
) : ViewModel() {

    private val _state = MutableStateFlow(ProductsState())
    val state: Flow<ProductsState> = _state

    init {
        viewModelScope.launch {
            observeUserUseCase().map { it.isSessionValid }.first()
            searchTermChanged("")
        }
    }

    @Suppress("UNCHECKED_CAST")
    fun searchTermChanged(term: CharSequence?) {
        Timber.d("invoked")
        if (!_state.value.loading) {
            term?.let { nonNullTerm ->
                val searchTerm = nonNullTerm.trim()

                val isEmptyTerm = searchTerm.isEmpty()
                if (isEmptyTerm && !_state.value.currentIsBlankSearchResult || !isEmptyTerm) {
                    Timber.d(searchTerm.toString())
                    viewModelScope.launch {
                        when (val result = searchProductsFromApiUseCase(searchTerm.toString())) {
                            is UseCaseResult.Failure -> {
                                _state.value = _state.value.copy(
                                    loading = false,
                                    currentIsBlankSearchResult = isEmptyTerm
                                )
                            }
                            is UseCaseResult.Success<*> -> {
                                if (result.value is List<*>) {
                                    _state.value =
                                        _state.value.copy(
                                            productItems = (result.value as List<Product>).map {
                                                ProductItem(it)
                                            },
                                            loading = false,
                                            currentIsBlankSearchResult = isEmptyTerm
                                        )
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    fun getSpanCount(isLandscape: Boolean, yInches: Float, xInches: Float): Int {
        var spanCount = 2
        if (isLandscape) {
            spanCount++
        }
        val diagonalInches = sqrt((xInches * xInches + yInches * yInches).toDouble())
        val isTablet = diagonalInches > 6.5
        if (isTablet) {
            spanCount++
        }
        Timber.d(spanCount.toString())
        return spanCount
    }
}
