package com.appstand.erply.ui.login

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.appstand.erply.usecase.*
import com.appstand.erply.utils.isValidId
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.launch
import timber.log.Timber
import javax.inject.Inject

@HiltViewModel
class LoginViewModel @Inject constructor(
    private val logInUserUseCase: LogInUserUseCase,
    private val saveCredentialsUseCase: SaveCredentialsUseCase,
    private val getSavedCredentialsUseCase: GetSavedCredentialsUseCase
) : ViewModel() {

    private val _loginError = MutableSharedFlow<Unit>(Integer.MAX_VALUE)
    val loginError: Flow<Unit> = _loginError
    private val _loginSuccess = MutableSharedFlow<Unit>(Integer.MAX_VALUE)
    val loginSuccess: Flow<Unit> = _loginSuccess
    private val _credentialsFiller =
        MutableSharedFlow<SavedCredentialsResult>(replay = Integer.MAX_VALUE)
    val credentialsFiller: Flow<SavedCredentialsResult> = _credentialsFiller
    private val _state = MutableStateFlow(LoginState())
    val state: Flow<LoginState> = _state

    init {
        viewModelScope.launch {
            when (val result = getSavedCredentialsUseCase()) {
                is UseCaseResult.Failure -> {
                }
                is UseCaseResult.Success<*> -> {
                    Timber.d("Success!")
                    if (result.value is SavedCredentialsResult) {
                        Timber.d("Success2!")
                        _credentialsFiller.emit(result.value)
                    }
                }
            }
        }
    }

    fun loginClicked(id: CharSequence?, username: CharSequence?, password: CharSequence?) {
        Timber.d("id: $id, email: $username")
        if (!_state.value.loading) {
            val isValidId = id.isValidId()
            val usernameError = username.isNullOrEmpty()
            val passwordError = password.isNullOrBlank()
            _state.value = _state.value.copy(
                idError = !isValidId,
                usernameError = usernameError,
                passwordError = passwordError
            )
            // no real password or username validation. do not know the rules for them..
            if (isValidId && !usernameError && !passwordError) {
                _state.value = _state.value.copy(loading = true)
                viewModelScope.launch {
                    val idString = id.toString()
                    val usernameString = username.toString()
                    val passwordString = password.toString()
                    val result =
                        logInUserUseCase(idString, usernameString, passwordString)
                    _state.value = _state.value.copy(loading = false)
                    when (result) {
                        is UseCaseResult.Failure -> _loginError.emit(Unit)
                        is UseCaseResult.Success<*> -> {
                            saveCredentialsUseCase(idString, usernameString, passwordString)
                            _loginSuccess.emit(Unit)
                        }
                    }
                }
            }
        }
    }
}
