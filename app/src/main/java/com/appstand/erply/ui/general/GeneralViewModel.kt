package com.appstand.erply.ui.general

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.appstand.erply.usecase.LogOutUserUseCase
import com.appstand.erply.usecase.ObserveUserUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.launch
import timber.log.Timber
import javax.inject.Inject

@HiltViewModel
class GeneralViewModel @Inject constructor(
    observeUserUseCase: ObserveUserUseCase,
    private val logOutUserUseCase: LogOutUserUseCase
) : ViewModel() {

    private val _logOut = MutableSharedFlow<Unit>(replay = Integer.MAX_VALUE)
    val logOut: Flow<Unit> = _logOut

    init {
        observeUserUseCase().onEach {
            Timber.d(it.toString())
            if (!it.isSessionValid) {
                Timber.d("Logging out!")
                _logOut.emit(Unit)
            }
        }.launchIn(viewModelScope)
    }

    fun logoutClicked() {
        viewModelScope.launch {
            logOutUserUseCase()
            Timber.d("Success!")
        }
    }
}
