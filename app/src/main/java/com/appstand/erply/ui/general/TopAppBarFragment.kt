package com.appstand.erply.ui.general

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.SearchView
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import com.appstand.erply.R
import com.appstand.erply.databinding.FragmentTopAppBarBinding
import com.appstand.erply.utils.getColorByAttribute
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import timber.log.Timber

@AndroidEntryPoint
class TopAppBarFragment : Fragment() {

    private val viewModel: GeneralViewModel by viewModels()
    private var _binding: FragmentTopAppBarBinding? = null

    private val binding get() = _binding!!

    var originalStatusBarColor: Int? = null

    private val _searchText = MutableStateFlow("")
    val searchText: Flow<String> = _searchText

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentTopAppBarBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onStart() {
        super.onStart()
        setup()
        observeViewModel()
    }

    private fun setup() {
        originalStatusBarColor = requireActivity().window.statusBarColor
        requireActivity().window.statusBarColor =
            requireContext().getColorByAttribute(R.attr.colorPrimary)
        val searchView = binding.topAppBar.menu.findItem(R.id.search).actionView as SearchView
        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                return false
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                newText?.let { _searchText.value = it }
                return true
            }
        })
        binding.topAppBar.menu.findItem(R.id.logOut).setOnMenuItemClickListener {
            viewModel.logoutClicked()
            true
        }
    }

    private fun observeViewModel() {
        viewModel.logOut.onEach {
            Timber.d("Logging out!")
            originalStatusBarColor?.let { requireActivity().window.statusBarColor = it }
            findNavController().navigate(R.id.toLoginFragment_logOut)
        }.launchIn(lifecycleScope)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}
