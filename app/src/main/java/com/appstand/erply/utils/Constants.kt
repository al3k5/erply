package com.appstand.erply.utils

import androidx.datastore.preferences.core.longPreferencesKey
import androidx.datastore.preferences.core.stringPreferencesKey

const val SESSION_EXPIRATION_TIME = 86400
const val REQUEST_VERIFY_USER = "verifyUser"
const val REQUEST_GET_SESSION_KEY = "getSessionKeyUser"

const val DATASTORE_NAME = "datastore"
const val DATASTORE_LOGGED_IN_KEY = "logged_in_key"
const val DATASTORE_LOGGED_IN_CLIENT_CODE = "client_code"
const val DATASTTORE_LOGGED_IN_EXPIRATION_TIME = "expiration_time"
const val DATASTORE_LAST_ID = "last_id"
const val DATASTORE_LAST_USERNAME = "last_username"
const val DATASTORE_LAST_PASSWORD = "last_password"

@JvmField
val DATASTORE_LOGGED_IN_SESSION_KEY = stringPreferencesKey(DATASTORE_LOGGED_IN_KEY)

@JvmField
val DATASTORE_LOGGED_IN_CLIENT_CODE_KEY = stringPreferencesKey(DATASTORE_LOGGED_IN_CLIENT_CODE)

@JvmField
val DATASTORE_LOGGED_IN_EXPIRATION_TIME_KEY =
    longPreferencesKey(DATASTTORE_LOGGED_IN_EXPIRATION_TIME)

@JvmField
val DATASTORE_LAST_ID_KEY = stringPreferencesKey(DATASTORE_LAST_ID)

@JvmField
val DATASTORE_LAST_USERNAME_KEY = stringPreferencesKey(DATASTORE_LAST_USERNAME)

@JvmField
val DATASTORE_LAST_PASSWORD_KEY = stringPreferencesKey(DATASTORE_LAST_PASSWORD)

const val TOKEN_WORK_NAME = "token_refreshing"

