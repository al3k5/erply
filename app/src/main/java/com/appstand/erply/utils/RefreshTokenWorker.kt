package com.appstand.erply.utils

import android.content.Context
import androidx.hilt.work.HiltWorker
import androidx.work.CoroutineWorker
import androidx.work.WorkerParameters
import com.appstand.erply.usecase.RefreshUserUseCase
import com.appstand.erply.usecase.UseCaseResult
import dagger.assisted.Assisted
import dagger.assisted.AssistedInject

@HiltWorker
class RefreshTokenWorker @AssistedInject constructor(
    @Assisted appContext: Context,
    @Assisted workerParams: WorkerParameters,
    private val refreshUserUseCase: RefreshUserUseCase
) : CoroutineWorker(appContext, workerParams) {

    override suspend fun doWork(): Result {
        val result = refreshUserUseCase.invoke()
        if (result is UseCaseResult.Success<*>) {
            return Result.success()
        } else {
            return Result.failure()
        }
    }
}
