package com.appstand.erply.utils

import android.content.Context
import androidx.datastore.preferences.core.Preferences
import androidx.datastore.preferences.core.edit
import com.appstand.erply.data.User
import dagger.hilt.android.qualifiers.ApplicationContext
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
open class DatastoreManager @Inject constructor(
    @ApplicationContext private val context: Context
) {

    open suspend fun <T> addToDatastore(key: Preferences.Key<T>, value: T) {
        context.dataStore.edit { settings ->
            settings[key] = value
        }
    }

    fun <T> observeKeyValue(key: Preferences.Key<T>): Flow<T?> {
        return context.dataStore.data.map { settings ->
            settings[key]
        }
    }

    suspend fun removeUser() {
        context.dataStore.edit { settings ->
            settings.remove(DATASTORE_LOGGED_IN_CLIENT_CODE_KEY)
            settings.remove(DATASTORE_LOGGED_IN_EXPIRATION_TIME_KEY)
            settings.remove(DATASTORE_LOGGED_IN_SESSION_KEY)
        }
    }

    fun observeUser(): Flow<User> {
        return context.dataStore.data.map { settings ->
            val key = settings[DATASTORE_LOGGED_IN_SESSION_KEY]
            val clientCode = settings[DATASTORE_LOGGED_IN_CLIENT_CODE_KEY]
            val expirationTime = settings[DATASTORE_LOGGED_IN_EXPIRATION_TIME_KEY]
            User(
                clientCode = clientCode ?: "",
                sessionKey = key ?: "",
                expirationTime = expirationTime ?: Long.MIN_VALUE
            )
        }
    }
}
