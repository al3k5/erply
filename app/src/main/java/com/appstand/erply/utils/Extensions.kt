package com.appstand.erply.utils

import android.content.Context
import android.view.View
import androidx.annotation.AttrRes
import androidx.core.content.res.getColorOrThrow
import androidx.core.content.withStyledAttributes
import androidx.core.text.isDigitsOnly
import androidx.datastore.core.DataStore
import androidx.datastore.preferences.core.Preferences
import androidx.datastore.preferences.preferencesDataStore
import com.google.android.material.snackbar.Snackbar

fun String.toErplyUrl(): String {
    return "https://$this.erply.com/api/"
}

val Context.dataStore: DataStore<Preferences> by preferencesDataStore(name = DATASTORE_NAME)

fun CharSequence?.isValidId(): Boolean =
    !isNullOrEmpty() && isDigitsOnly() && length <= 6 && length >= 4

fun Context.getColorByAttribute(@AttrRes attr: Int): Int {
    withStyledAttributes(0, intArrayOf(attr)) {
        return getColorOrThrow(0)
    }
    throw IllegalArgumentException()
}

fun Context.showSnackbar(
    message: String,
    backgroundTint: Int,
    view: View
) {
    Snackbar
        .make(
            this,
            view,
            message,
            Snackbar.LENGTH_LONG
        )
        .setBackgroundTint(backgroundTint)
        .show()
}
