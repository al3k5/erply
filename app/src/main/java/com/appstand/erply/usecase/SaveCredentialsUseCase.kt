package com.appstand.erply.usecase

import com.appstand.erply.utils.DATASTORE_LAST_ID_KEY
import com.appstand.erply.utils.DATASTORE_LAST_PASSWORD_KEY
import com.appstand.erply.utils.DATASTORE_LAST_USERNAME_KEY
import com.appstand.erply.utils.DatastoreManager
import timber.log.Timber
import javax.inject.Inject

class SaveCredentialsUseCase @Inject constructor(
    private val datastoreManager: DatastoreManager
) {

    suspend operator fun invoke(id: String, userName: String, password: String) {
        try {
            datastoreManager.addToDatastore(
                DATASTORE_LAST_ID_KEY,
                id
            )
            datastoreManager.addToDatastore(
                DATASTORE_LAST_USERNAME_KEY, userName
            )
            datastoreManager.addToDatastore(
                DATASTORE_LAST_PASSWORD_KEY,
                password
            )
            Timber.d("Success!")
        } catch (e: Exception) {
            Timber.e(e)
        }
    }
}
