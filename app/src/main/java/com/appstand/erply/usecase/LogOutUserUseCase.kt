package com.appstand.erply.usecase

import com.appstand.erply.utils.DatastoreManager
import timber.log.Timber
import javax.inject.Inject

open class LogOutUserUseCase @Inject constructor(
    private val datastoreManager: DatastoreManager
) {

    open suspend operator fun invoke() {
        try {
            datastoreManager.removeUser()
            Timber.d("Success!")
        } catch (e: Exception) {
            Timber.e(e)
        }
    }
}
