package com.appstand.erply.usecase

import com.appstand.erply.utils.DATASTORE_LAST_ID_KEY
import com.appstand.erply.utils.DATASTORE_LAST_PASSWORD_KEY
import com.appstand.erply.utils.DATASTORE_LAST_USERNAME_KEY
import com.appstand.erply.utils.DatastoreManager
import kotlinx.coroutines.flow.first
import timber.log.Timber
import javax.inject.Inject

class GetSavedCredentialsUseCase @Inject constructor(
    private val datastoreManager: DatastoreManager
) {

    suspend operator fun invoke(): UseCaseResult {
        try {
            val id = datastoreManager.observeKeyValue(DATASTORE_LAST_ID_KEY).first()
            val userName = datastoreManager.observeKeyValue(DATASTORE_LAST_USERNAME_KEY).first()
            val password = datastoreManager.observeKeyValue(DATASTORE_LAST_PASSWORD_KEY).first()
            if (id != null && userName != null && password != null) {
                Timber.d("Success!")
                return UseCaseResult.Success(SavedCredentialsResult(id, userName, password))
            } else {
                Timber.d("Failure!")
                return UseCaseResult.Failure
            }
        } catch (e: Exception) {
            Timber.e("Failure!\n ${e.localizedMessage}")
            return UseCaseResult.Failure
        }
    }
}

data class SavedCredentialsResult(
    val id: String,
    val userName: String,
    val password: String
)
