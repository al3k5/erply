package com.appstand.erply.usecase

import com.appstand.erply.data.api.VerificationService
import com.appstand.erply.utils.DATASTORE_LOGGED_IN_EXPIRATION_TIME_KEY
import com.appstand.erply.utils.DATASTORE_LOGGED_IN_SESSION_KEY
import com.appstand.erply.utils.DatastoreManager
import com.appstand.erply.utils.toErplyUrl
import kotlinx.coroutines.flow.first
import timber.log.Timber
import javax.inject.Inject

class RefreshUserUseCase @Inject constructor(
    private val datastoreManager: DatastoreManager,
    private val verificationService: VerificationService
) {

    suspend operator fun invoke(): UseCaseResult {
        try {
            val user = datastoreManager.observeUser().first()
            if (!user.isSessionValid) {
                return UseCaseResult.Failure
            }
            val record =
                verificationService.getSessionKeyUser(
                    url = user.clientCode.toErplyUrl(),
                    sessionKey = user.sessionKey,
                    clientCode = user.clientCode
                ).records.firstOrNull()
            if (record != null) {
                datastoreManager.addToDatastore(
                    DATASTORE_LOGGED_IN_EXPIRATION_TIME_KEY,
                    System.currentTimeMillis() + record.sessionLength * 1000
                )
                datastoreManager.addToDatastore(
                    DATASTORE_LOGGED_IN_SESSION_KEY,
                    record.sessionKey
                )
                Timber.d("Success!")
                return UseCaseResult.Success(null)
            } else {
                throw Exception("Missing records!")
            }
        } catch (e: Exception) {
            Timber.e(e)
            return UseCaseResult.Failure
        }
    }
}
