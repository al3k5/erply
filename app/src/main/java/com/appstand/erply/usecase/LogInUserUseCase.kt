package com.appstand.erply.usecase

import com.appstand.erply.data.api.VerificationService
import com.appstand.erply.utils.*
import timber.log.Timber
import javax.inject.Inject

class LogInUserUseCase @Inject constructor(
    private val verificationService: VerificationService,
    private val datastoreManager: DatastoreManager
) {

    suspend operator fun invoke(
        clientCode: String,
        userName: String,
        password: String
    ): UseCaseResult {
        return try {
            val record = verificationService.verifyUser(
                clientCode.toErplyUrl(),
                userName,
                password,
                clientCode
            ).records.firstOrNull()
            if (record != null) {
                datastoreManager.addToDatastore(
                    DATASTORE_LOGGED_IN_EXPIRATION_TIME_KEY,
                    System.currentTimeMillis() + record.sessionLength * 1000
                )
                datastoreManager.addToDatastore(
                    DATASTORE_LOGGED_IN_SESSION_KEY,
                    record.sessionKey
                )
                datastoreManager.addToDatastore(
                    DATASTORE_LOGGED_IN_CLIENT_CODE_KEY,
                    clientCode
                )
                Timber.d("Success!")
                return UseCaseResult.Success(null)
            } else {
                throw Exception("Missing records!")
            }
        } catch (e: Exception) {
            Timber.e(e)
            UseCaseResult.Failure
        }
    }
}
