package com.appstand.erply.usecase

sealed class UseCaseResult {
    data class Success<T>(val value: T?) : UseCaseResult()
    object Failure : UseCaseResult()
}
