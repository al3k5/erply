package com.appstand.erply.usecase

import com.appstand.erply.data.api.ProductService
import timber.log.Timber
import javax.inject.Inject

class SearchProductsFromApiUseCase @Inject constructor(
    private val productService: ProductService
) {

    suspend operator fun invoke(term: String): UseCaseResult {
        try {
            val products = productService.getProducts(
                "[[\"name.en\",\"contains\",\"$term\"]]"
            )
            Timber.d("Success: $products")
            return UseCaseResult.Success(products)
        } catch (e: Exception) {
            Timber.e(e)
            return UseCaseResult.Failure
        }
    }
}
