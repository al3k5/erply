package com.appstand.erply.usecase

import com.appstand.erply.data.User
import com.appstand.erply.utils.DatastoreManager
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

open class ObserveUserUseCase @Inject constructor(
    private val datastoreManager: DatastoreManager
) {

    open operator fun invoke(): Flow<User> {
        return datastoreManager.observeUser()
    }
}
