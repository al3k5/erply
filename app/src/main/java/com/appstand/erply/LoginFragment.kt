package com.appstand.erply

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isInvisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import com.appstand.erply.databinding.FragmentLoginBinding
import com.appstand.erply.ui.login.LoginViewModel
import com.appstand.erply.utils.getColorByAttribute
import com.appstand.erply.utils.showSnackbar
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach

@AndroidEntryPoint
class LoginFragment : Fragment() {

    private val viewModel: LoginViewModel by viewModels()
    private var _binding: FragmentLoginBinding? = null

    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentLoginBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setup()
        observeViewModel()
    }

    private fun setup() {
        binding.loginButton.setOnClickListener {
            viewModel.loginClicked(
                binding.idEditText.text,
                binding.usernameEditText.text,
                binding.passwordEditText.text
            )
        }
    }

    private fun observeViewModel() {
        with(lifecycleScope) {
            viewModel.state.onEach { state ->
                binding.idLayout.error =
                    if (state.idError) {
                        resources.getString(R.string.login_invalid_id)
                    } else {
                        null
                    }
                binding.loadingIndicator.isInvisible = !state.loading
            }.launchIn(this)

            viewModel.loginSuccess.onEach {
                findNavController().navigate(R.id.toProductsFragment)
            }.launchIn(this)

            viewModel.loginError.onEach {
                requireContext()
                    .showSnackbar(
                        resources.getString(R.string.login_failed),
                        requireContext().getColorByAttribute(R.attr.colorError),
                        binding.root
                    )
            }.launchIn(this)

            viewModel.credentialsFiller.onEach { savedCreds ->
                binding.idEditText.setText(savedCreds.id)
                binding.usernameEditText.setText(savedCreds.userName)
                binding.passwordEditText.setText(savedCreds.password)
            }.launchIn(this)
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}
