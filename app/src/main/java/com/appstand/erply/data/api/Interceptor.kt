package com.appstand.erply.data.api

import com.appstand.erply.data.User
import com.appstand.erply.usecase.LogOutUserUseCase
import com.appstand.erply.usecase.ObserveUserUseCase
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.SupervisorJob
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.launch
import kotlinx.serialization.decodeFromString
import kotlinx.serialization.json.Json
import okhttp3.Interceptor
import okhttp3.Response
import timber.log.Timber
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class Interceptor @Inject constructor(
    observeUserUseCase: ObserveUserUseCase,
    private val logOutUserUseCase: LogOutUserUseCase,
    private val json: Json
) : Interceptor {

    // should be fine since its a singleton that will be destroyed when app is destroyed
    private val applicationScope = CoroutineScope(SupervisorJob() + Dispatchers.Main)
    private var user: User? = null

    init {
        observeUserUseCase().onEach { user = it }.launchIn(applicationScope)
    }

    override fun intercept(chain: Interceptor.Chain): Response {
        val builder = chain.request().newBuilder()
        val requestUrl = chain.request().url.toString()

        // request handling
        user?.let { user ->
            // not the login endpoint
            if (!requestUrl.contains(user.clientCode)) {
                if (user.isSessionValid) {
                    builder.addHeader("sessionKey", user.sessionKey)
                    builder.addHeader("clientCode", user.clientCode)
                } else {
                    applicationScope.launch {
                        logOutUserUseCase()
                    }
                }
            }
        }

        // response handling
        val response = chain.proceed(builder.build())

        if (response.isSuccessful) {
            user?.let { user ->
                if (user.clientCode.isNotEmpty() && requestUrl.contains(user.clientCode)) {
                    val body = response.peekBody(Long.MAX_VALUE)
                    body.string().let {
                        Timber.d("Trying intercept response")
                        val obj = json.decodeFromString<VerificationResponse>(it)
                        Timber.d(obj.toString())
                        val errorCode = obj.status.errorCode
                        if (errorCode == 1055 || errorCode == 1054 || errorCode == 1009) {
                            applicationScope.launch {
                                logOutUserUseCase()
                            }
                        }
                    }
                    body.close()
                }
            }
        }
        return response
    }
}
