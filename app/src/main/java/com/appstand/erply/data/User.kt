package com.appstand.erply.data

data class User(
    val clientCode: String,
    val sessionKey: String,
    val expirationTime: Long
) {
    val isSessionValid: Boolean
        get() = expirationTime > System.currentTimeMillis()
}
