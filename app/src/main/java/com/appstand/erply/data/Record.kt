package com.appstand.erply.data

import kotlinx.serialization.Serializable

@Serializable
data class Record(
    val sessionKey: String,
    val sessionLength: Int
)
