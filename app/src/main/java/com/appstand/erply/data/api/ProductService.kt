package com.appstand.erply.data.api

import com.appstand.erply.data.Product
import retrofit2.http.GET
import retrofit2.http.Query

interface ProductService {

    @GET("product/")
    suspend fun getProducts(
        @Query("filter") filter: String,
        @Query("take") take: Int = 100
    ): List<Product>
}
