package com.appstand.erply.data.api

import com.appstand.erply.data.Record
import com.appstand.erply.utils.REQUEST_GET_SESSION_KEY
import com.appstand.erply.utils.REQUEST_VERIFY_USER
import com.appstand.erply.utils.SESSION_EXPIRATION_TIME
import kotlinx.serialization.Serializable
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.POST
import retrofit2.http.Url

interface VerificationService {

    @FormUrlEncoded
    @POST
    suspend fun verifyUser(
        @Url url: String,
        @Field(value = "username") username: String,
        @Field(value = "password") password: String,
        @Field(value = "clientCode") clientCode: String,
        @Field(value = "request") request: String = REQUEST_VERIFY_USER,
        @Field(value = "sendContentType") sendContentType: Int = 1,
        @Field(value = "sessionLength") sessionLength: Int = SESSION_EXPIRATION_TIME
    ): VerificationResponse

    @FormUrlEncoded
    @POST
    suspend fun getSessionKeyUser(
        @Url url: String,
        @Field(value = "request") request: String = REQUEST_GET_SESSION_KEY,
        @Field(value = "sessionKey") sessionKey: String,
        @Field(value = "clientCode") clientCode: String
    ): VerificationResponse
}

@Serializable
data class VerificationResponse(val records: List<Record> = listOf(), val status: Status)

@Serializable
data class Status(
    val errorCode: Int
)
