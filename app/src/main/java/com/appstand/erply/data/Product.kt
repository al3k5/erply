package com.appstand.erply.data

import kotlinx.serialization.Serializable

@Serializable
data class Product(
    val id: Int,
    val name: Name
)

@Serializable
data class Name(
    val en: String
)
