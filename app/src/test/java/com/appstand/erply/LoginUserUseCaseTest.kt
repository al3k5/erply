package com.appstand.erply

import com.appstand.erply.data.Record
import com.appstand.erply.data.api.Status
import com.appstand.erply.data.api.VerificationResponse
import com.appstand.erply.data.api.VerificationService
import com.appstand.erply.usecase.LogInUserUseCase
import com.appstand.erply.usecase.UseCaseResult
import com.appstand.erply.utils.DatastoreManager
import junit.framework.TestCase.assertEquals
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Before
import org.junit.Test
import org.mockito.Mockito.mock

/**
 * Example of how unit test use cases
 */
@ExperimentalCoroutinesApi
class LoginUserUseCaseTest {

    private lateinit var verificationService: VerificationService
    private lateinit var datastoreManager: DatastoreManager

    @Before
    fun setup() {
        datastoreManager = mock(DatastoreManager::class.java)
    }

    @Test
    fun success() {
        val requestPassword = "password"
        val requestUsername = "username"
        val requestClientCode = "123456"

        val responseSessionKey = "key"
        val responseSessionLength = 1
        runBlockingTest {

            verificationService = mock(
                VerificationService::class.java
            ) {
                VerificationResponse(
                    listOf(
                        Record(responseSessionKey, responseSessionLength)
                    ),
                    Status(0)
                )
            }

            val useCase = LogInUserUseCase(verificationService, datastoreManager)

            assertEquals(
                UseCaseResult.Success(null),
                useCase.invoke(
                    clientCode = requestClientCode,
                    userName = requestUsername,
                    password = requestPassword
                )
            )
        }
    }

    @Test
    fun failure() {
        runBlockingTest {
            verificationService = mock(
                VerificationService::class.java
            ) {
                throw Exception("nope")
            }
            val useCase = LogInUserUseCase(verificationService, datastoreManager)

            assertEquals(
                UseCaseResult.Failure,
                useCase.invoke(
                    clientCode = "asjnka",
                    userName = "ajsna",
                    password = "asnjka"
                )
            )
        }
    }
}
