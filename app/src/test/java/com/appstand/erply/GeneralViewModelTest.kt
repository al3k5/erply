package com.appstand.erply

import app.cash.turbine.test
import com.appstand.erply.data.User
import com.appstand.erply.ui.general.GeneralViewModel
import com.appstand.erply.usecase.LogOutUserUseCase
import com.appstand.erply.usecase.ObserveUserUseCase
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.runBlockingTest
import kotlinx.coroutines.test.setMain
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.mockito.Mockito.*
import kotlin.time.ExperimentalTime

/**
 * Example of how unit test viewModels
 */
@ExperimentalTime
@ExperimentalCoroutinesApi
class GeneralViewModelTest {

    private lateinit var observeUserUseCase: ObserveUserUseCase
    private lateinit var logoutUserUseCase: LogOutUserUseCase

    @Before
    fun setup() {
        Dispatchers.setMain(Dispatchers.Unconfined)
        observeUserUseCase = mock(ObserveUserUseCase::class.java)
        logoutUserUseCase = mock(LogOutUserUseCase::class.java)
    }

    @After
    fun tearDown() {
        Dispatchers.resetMain()
    }

    @Test
    fun init_sessionExpired() {
        `when`(observeUserUseCase.invoke()).thenReturn(flow {
            emit(User("", "", System.currentTimeMillis() - 1000L))
        })
        // inject
        val viewModel = GeneralViewModel(observeUserUseCase, logoutUserUseCase)
        // test
        runBlockingTest {
            viewModel.logOut.test {
                expectItem()
            }
        }
    }

    @Test
    fun init_sessionNotExpired() {
        `when`(observeUserUseCase.invoke()).thenReturn(flow {
            emit(User("", "", System.currentTimeMillis() + 1000L))
        })
        // inject
        val viewModel = GeneralViewModel(observeUserUseCase, logoutUserUseCase)
        // test
        runBlockingTest {
            viewModel.logOut.test {
                expectNoEvents()
            }
        }
    }

    @Test
    fun logoutClicked_logOutUseCaseCalled() {
        `when`(observeUserUseCase.invoke()).thenReturn(flow {
            emit(User("", "", System.currentTimeMillis() + 1000L))
        })
        val viewModel = GeneralViewModel(observeUserUseCase, logoutUserUseCase)
        viewModel.logoutClicked()
        runBlockingTest {
            verify(logoutUserUseCase).invoke()
        }
    }
}
