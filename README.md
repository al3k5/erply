# Erply
Android test assignment for Erply.

# Description
App opens to show login form if user has not authenticated yet. User can input their client code, username and password.
Upon succesful authentication they are taken to the product search screen.
Works on phone, tablet. In portrait, landscape.

## Logging in
User can input:
 1. client code - form validates to see if the input is between 4 and 6 characters.
 2. username - no validation, as it is unclear if this is only email or maybe some other format.
 3. password - no validation.
If validation passes then the request is done to the api. If all is well then log in saves the API credentials.
Succesful credentials are stored for future ease of use. See when logging in again.
If there is an error, a snackbar is shown.

## Token handling
An interceptor takes care of adding the sessionKey and clientCode to subsequent requests.
If at any point the token expires, then the user is logged out.
A workmanager takes care of refreshing the token every hour.

## Product search
User can input search terms into the app bar's search view. Requests are debounced every 300ms.
Grid shows 2 columns - mobile, 3 columns - mobile landscape, tablet portrait, 4 columns - tablet landscape. 
The assignment was to show something similar to designs sent. However it seems the API does
not return images. To show a similar design showing broken image sign until images become available.

# Tech stack
Exact stack can be seen in build.gradle.kts

## General architecture
Application layer for dependency pool. These are injected where needed using Hilt.
ViewModels for view controlling and state. 
ViewModels rely on use cases to do any complex long going operations.

## App
1. Hilt - dependency injection.
2. Viewmodels - retain state.
3. Kotlin coroutines, flow - long going operations.
4. Workmanager - background operations.
5. Datastore - to store any keyvalue pairs. 

## Testing
1. Flow turbine library for testing kotlin flows.
Did not write a lot of tests, just examples of how things can be tested.
